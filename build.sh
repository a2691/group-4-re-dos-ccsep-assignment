#!/bin/bash

sudo apt update
sudo install python3.8
python3.8 --version

sudo apt install python3-pip

sudo pip install flask

python3 -c "import flask; print(flask.__version__)"
