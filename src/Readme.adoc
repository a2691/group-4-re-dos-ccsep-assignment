== Step-By-Step Guide on triggering and exploiting vulnerability

1. To build the program, go to directory gitlab directory you cloned or downloaded, and type the command [./build.sh].
2. To run the program, type the command [./run.sh].
3. Use any web browser and key in the link provided when running the "run.sh" script into the search bar.
4. A register page will load where you will enter any username and password. If the username is used within the password or vice versa, the page will send an error. Only when it is entirely different, only then the page will log the user into the home page.
5. To detect whether the ReDOS is avaliable, you can enter a simple regex into the username and a password that matches the regex, if it accepts the regex, you would be able to register and enter the website with no problem.
6. To exploit it, you can enter an evil regex(^(a+)+$) into the username and a password(aaaaaaaaaaaaaaaaaaaaaaaaaaq) that responds to that evil regex which will trigger the browser to keep loading and checking the username and passwords.
7. This makes the server to the web application busy serving the evil regex account and other users who tries to enter the web application is unable to.
8. Therefore, ReDOS on a small scale has been performed!!!
