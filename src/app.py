from flask import Flask, render_template, redirect, url_for, request
import re

app = Flask(__name__)


@app.route('/index')
def index():
	return render_template('index.html')


@app.route('/', methods=['GET', 'POST'])
def login():
	error = None
	if request.method == 'POST':	
		if (re.search(request.form['username'], request.form['password']) or re.search(request.form['password'], request.form['username'])):#true if username is in password
			error = 'Please do not use your username within your password and try again'
		else:
			return redirect(url_for('index'))
			
	return render_template('login.html', error=error)
	

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=8888, debug=True)
